#pragma once

#include "ofMain.h"
#include "EyeFace.h"

// Currently Windows-only
#include "Windows.h"


// path to a directory containing config.ini file and models
//#define EYEFACE_DIR    (char*)"../../../../addons/ofxEyeFace/libs/eyefacesdk"
#define EYEFACE_DIR    (char*)"eyefacesdk"
#define CONFIG_INI     (char*)"config.ini"

// EyeFace-SDK shared library selection - windows only at the time
#define _lib_path = "eyefacesdk\\lib\\EyeFace.dll";
//#define _lib_path = "..\..\\..\\..\\addons\\ofxEyeFace\\libs\\eyefacesdk\\lib\\EyeFace.dll";

struct FaceData {

	int             track_number;               //!< Track unique number. Track is a space-time aggregation of face detecions. */
    int             id;                         //!< Person identity unique number based on face recognition, 0 if not known yet.*/
                                                
    ofRectangle   current_position;           //!< Face position. Internally aggregated over past frames. */
    double          energy;                     //!< Fade-out energy: 1 if the face was detected in the current frame, less than 1 if not. This number is used for alpha channel of bounding-box visualisation (fade-out). */
    
    EfGender        gender;                     //!< Face gender. Internally aggregated over all past face frames. */
    double          gender_confidence;          //!< Classification response: -1 <= male < -0.2 <= unknown <= 0.2 < female <= 1. Internally aggregated over past frames. */
    double          age_response;               //!< Age clasification response, currently NOT USED. */ 
    int             age;                        //!< Age in years (or -1 for unknown). Internally aggregated over all past face frames. */
    
    int             logged;                     //!< Track status: 0 - a not verified track (face detected just in one frame), 1 or 2 - a verified track, face detected in 2 frames at least. */
    
    //Ef2dPoints8     landmarks;	                //!< Landmark points. Point 0-7 can be further stabilized using landmark_precise_use_precise (See the Developer's Guide). [0=FACE_CENTER 1=L_CANTHUS_R_EYE 2=R_CANTHUS_L_EYE 3=MOUTH_R 4=MOUTH_L 5=R_CANTHUS_R_EYE 6=L_CANTHUS_L_EYE 7=NOSE_TIP 8=L_EYEBROW_L 9=L_EYEBROW_C 10=L_EYEBROW_R 11=R_EYEBROW_L 12=R_EYEBROW_C 13=R_EYEBROW_R 14=NOSE_ROOT 15=NOSE_L 16=NOSE_R 17=MOUTH_T 18=MOUTH_B 19=CHIN]*/
	// note: landmarks is said to be deprecated in SDK, so I've did not wrapping around them.

    double          curr_position_x;            //!< 2D real-world position - X axis (left-right). Internally aggregated over past frames. */
    double          curr_position_y;            //!< 2D real-world position - Y axis (forward-backward). Internally aggregated over past frames.*/
	ofPoint			curr_position;
	double          angles[3];                  //!< Roll, Pitch, Yaw - only Yaw and Roll now used. */
    
    double          start_time;                 //!< Track start time [seconds]. */
    double          total_time;                 //!< Track duration [seconds].   */
    double          attention_time;             //!< Estimate of time [seconds] the person looked into camera. (See the Developer's Guide)*/
    int             attention_now;              //!< Estimate of whether the person looks in the camera in current frame. */
      
    int             detection_index;            //!< Use in Advanced API only if dEnergy == 1! Tells the index to EfDetResult structure outputted by efRunFaceDetector, so that EfVisualData can be matched with EfDetection. */

};

class ofxEyeFace {

// path to test image
//#define IMAGE_FILENAME (char*)"../../data/test-images-id/0998.jpg"


// global pointer to EyeFace-SDK shared library
ef_shlib_hnd lib_handle;

// Declaration of pointers to library functions
fcn_efReadImageUc                       fcnEfReadImageUc;
fcn_efFreeImageUc                       fcnEfFreeImageUc;
fcn_efInitEyeFace                       fcnEfInitEyeFace;
fcn_efFreeEyeFaceState                  fcnEfFreeEyeFaceState;
fcn_efMain                              fcnEfMain;
fcn_efGetVisualOutput                   fcnEfGetVisualOutput;
fcn_efFreeVisualOutput                  fcnEfFreeVisualOutput;
fcn_efGetLibraryVersion                 fcnEfGetLibraryVersion;

// engine state
void * state;
unsigned char *raw_img;
ofPixels pixels;
EfMatrixUc * image;


double lastTimeProcessed;

public:

	float min_stability;
	int visible_at_least;

	vector<FaceData*> results;

	string report;
	EfMatrixUc * input_image;
	ofxEyeFace() {
		state = NULL;

		min_stability = 0.5;
		visible_at_least = 1;
	};

	void process(ofTexture &data) {
		//if (!raw_img) {
			//ofLog() << "initializing raw_img :" << data.getWidth() << " : " << data.getHeight();
			raw_img = new unsigned char[data.getWidth() * data.getHeight()];
		//}
		int cnt = 0;
		data.readToPixels(pixels);
/*		for (int i = 0; i < data.getWidth(); i++) {
			for (int j = 0; j < data.getHeight(); j++) {
				raw_img[cnt++] = 0;//pixels.getColor(i, j).r;
				raw_img[cnt++] = 0;//pixels.getColor(i, j).g;
				raw_img[cnt++] = 0;//pixels.getColor(i, j).b;
			}
		}*/

		EfBoundingBox bounding_box;
        bounding_box.top_left_col = 0;  
        bounding_box.top_left_row = 0;  
		bounding_box.bot_right_col = data.getWidth() - 1;
        bounding_box.bot_right_row = data.getHeight() - 1;

		image = new EfMatrixUc;
		image->no_cols = data.getWidth();
		image->no_rows = data.getHeight();
		cnt = 0;
		image->data = new unsigned char[image->no_cols * image->no_rows];
		//ofLog() << "data size is " << sizeof(raw_img);
		for (int i = 0; i < data.getHeight(); i++) {
			for (int j = 0; j < data.getWidth(); j++) {
				image->data[cnt++] = pixels.getColor(j, i).r;// * pixels.getColor(j, i).g * pixels.getColor(j, i).b; 
			}
		}


		fcnEfMain((unsigned char*)image->data, image->no_cols,
			image->no_rows, 0, &bounding_box, state, (ofGetElapsedTimef()-lastTimeProcessed));


		/*
		if (fcnEfMain(raw_img,data.getWidth(),data.getHeight(),
			0,&bounding_box,eyeface_state,ofGetElapsedTimef()-lastTimeProcessed))
        {
			ofLog() << "detection failed!!";
        }*/

        ////////////////////////////////////////
        // get Visual Output                    
        ////////////////////////////////////////
        EfVisualOutput * visual_output = NULL;
//        double min_stability    = 0.8; // show detections/tracks with detection/missdetection from min_stability
//        int    visible_at_least  = 1;   // show only detections/tracks visible at leaset visible_at_least frames
        if (!(visual_output = fcnEfGetVisualOutput(state, min_stability, visible_at_least))) 
        {
            ofLog() << "Running efGetVisualOutput() failed.\n";
        }   

		//ofLog() << "people found:" << visual_output->num_vis;

		report = "";
		while (!results.empty()){
			FaceData* result = results.back();
			results.pop_back();
			delete result;
		}

		for (int i = 0; i < visual_output->num_vis; i++)
		{
			FaceData *result = new FaceData;
			result->track_number = visual_output->vis_data[i].track_number;
			result->id = visual_output->vis_data[i].id;
			result->current_position = ofRectangle(
				visual_output->vis_data[i].current_position.top_left_col,
				visual_output->vis_data[i].current_position.top_left_row,
				visual_output->vis_data[i].current_position.bot_right_col -
					visual_output->vis_data[i].current_position.top_left_col,
				visual_output->vis_data[i].current_position.bot_right_row -
					visual_output->vis_data[i].current_position.top_left_row);
			result->energy = visual_output->vis_data[i].energy;
			result->gender = visual_output->vis_data[i].gender;
			result->gender_confidence = visual_output->vis_data[i].gender_confidence;
			result->age_response = visual_output->vis_data[i].age_response;
			result->age = visual_output->vis_data[i].age;
			result->logged = visual_output->vis_data[i].logged;
			result->curr_position_x = visual_output->vis_data[i].curr_position_x;
			result->curr_position_y = visual_output->vis_data[i].curr_position_y;
			result->curr_position = ofPoint(visual_output->vis_data[i].curr_position_x,
											visual_output->vis_data[i].curr_position_y);
			result->start_time = visual_output->vis_data[i].start_time;
			result->total_time = visual_output->vis_data[i].total_time;
			result->attention_time = visual_output->vis_data[i].attention_time;
			result->attention_now = visual_output->vis_data[i].attention_now;

			result->detection_index = visual_output->vis_data[i].detection_index;

			results.push_back(result);

			report += "Person " + ofToString(visual_output->vis_data[i].id) + " : age " + ofToString(visual_output->vis_data[i].age);
            EfGender gender = visual_output->vis_data[i].gender;            
			report += ", ";
			if (gender == EF_GENDER_NONE) report += "unknown";
				else report += ( gender == EF_GENDER_MALE ? "male" : "female"  );
				report += ", attention time:" + ofToString(visual_output->vis_data[i].attention_time);
			report += "\n";
		}
		if (visual_output->num_vis == 0)
			report += "no people found.";

		//ofLog() << "result size:" << results.size();

		lastTimeProcessed = ofGetElapsedTimef();
		delete raw_img;
	}

	void setup() {
		ofLog() << "Setup started";

		lib_handle = NULL;
		
		//char text[] = "..\\..\\..\\..\\addons\\ofxEyeFace\\libs\\eyefacesdk\\lib\\EyeFace.dll";
		char text[] = "eyefacesdk\\lib\\EyeFace.dll";
		wchar_t wtext[67];
		mbstowcs(wtext, text, strlen(text)+1);//Plus null
		LPWSTR ptr = wtext;

		wcout << ptr;
		    // load shared library - explicit linking
		//lib_handle = LoadLibraryW(wstr);
		lib_handle = LoadLibrary(ptr);
		//EF_OPEN_SHLIB(lib_handle, lib_path);

	    // get pointers to functions from loaded library
	    EF_LOAD_SHFCN(fcnEfReadImageUc, fcn_efReadImageUc, lib_handle, "efReadImageUc");
	    EF_LOAD_SHFCN(fcnEfFreeImageUc, fcn_efFreeImageUc, lib_handle,  "efFreeImageUc");
	    EF_LOAD_SHFCN(fcnEfInitEyeFace, fcn_efInitEyeFace, lib_handle, "efInitEyeFace");
	    EF_LOAD_SHFCN(fcnEfFreeEyeFaceState, fcn_efFreeEyeFaceState, lib_handle, "efFreeEyeFaceState");
	    EF_LOAD_SHFCN(fcnEfMain, fcn_efMain, lib_handle, "efMain");
	    EF_LOAD_SHFCN(fcnEfGetVisualOutput, fcn_efGetVisualOutput, lib_handle, "efGetVisualOutput");
	    EF_LOAD_SHFCN(fcnEfFreeVisualOutput, fcn_efFreeVisualOutput, lib_handle, "efFreeVisualOutput");
	    EF_LOAD_SHFCN(fcnEfGetLibraryVersion, fcn_efGetLibraryVersion, lib_handle, "efGetLibraryVersion");

	    // write out library version
	    printf("EyeFace SDK library version: %d\n\n", fcnEfGetLibraryVersion());

		// library linking failed? (file not found, license invalid, or linker dependency missing)
		if (!lib_handle) {
			ofLog() << "Setup failed";
			return;
		}
		// init EyeFace state
		state = fcnEfInitEyeFace(EYEFACE_DIR,EYEFACE_DIR,EYEFACE_DIR,CONFIG_INI);

		// EyeFace SDK init failed? (license invalid) 
		if (!state) {
			ofLog() << "sdk init failed";
		} else ofLog() << "sdk init succeeded";

		/*
				ofLog() << ofSystem("dir");
				input_image = fcnEfReadImageUc("C:\\test_2.jpg");
				ofLog() << "no cols:" << input_image->no_cols;
				*/
				//ofLog() << " :::" << input_image->length;

				//ofLog() << "image loaded!";
				// load image
		 //EfMatrixUc * input_image = fcnEfReadImageUc(IMAGE_FILENAME);

	};
};
