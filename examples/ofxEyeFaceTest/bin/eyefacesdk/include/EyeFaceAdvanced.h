////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
///                                                              ///
///    Expert API header file of EyeFace SDK                     ///
///   --------------------------------------------------------   ///
///    The interface described here is usable with the Expert    ///
///    license only.                                             ///
///                                                              ///
///    Eyedea Recognition, Ltd. (C) 2014, Nov 20th               ///
///                                                              ///
///    Contact:                                                  ///
///               web: http://www.eyedea.cz                      ///
///             email: info@eyedea.cz                            ///
///                                                              ///
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////


#ifndef _EYE_FACE_ADVANCED_H
#define _EYE_FACE_ADVANCED_H

#include "EyeFace.h"
#include "EyeFaceTypeAdvanced.h"

/* /////////////////////////////////////////////////////////////// */
/*                                                                 */
/* ADVANCED EYE-FACE API:  SINGLE IMAGE PROCESSING                 */
/*                                                                 */
/* /////////////////////////////////////////////////////////////// */
/*! \addtogroup EyeFace
 @{
*/

/*! \defgroup Expert EyeFace Expert API
 @{
*/

/*! \fn EfDetResult * efRunFaceDetector(EfMatrixUc * image, void * module_state)
  \brief  Runs face detection on a single image. For video-sequence images expert manipulation and standalone images.
  \param  image Pointer to EfMatrixUc structure containing image data (8-bit unsigned char bitmap). 
  \param  module_state Pointer to EyeFace module state structure received by efInitEyeFace(). 
  \return Pointer to EfDetResult structure, NULL on failure.
*/
EYEFACE_FUNCTION_PREFIX EfDetResult * efRunFaceDetector(EfMatrixUc * image, void* module_state);

/*! \fn void efFreeDetResult(EfDetResult * det_result)
  \brief Frees EfDetResult structure returned by efRunFaceDetector().
  \param det_result Pointer to EfDetResult structure to be freed.
*/
EYEFACE_FUNCTION_PREFIX void efFreeDetResult(EfDetResult * det_result);

/*! \fn EfLandmarkResult * efRunFaceLandmark(EfMatrixUc * image, EfDetection * detection, void * module_state)
  \brief Runs face landmarks detection.
  \param image Pointer to EfMatrixUc structure containing image data (8-bit unsigned char bitmap).
  \param detection Pointer to face detection structure returned by efRunFaceDetector().
  \param  module_state Pointer to EyeFace module state structure received by efInitEyeFace().
  \return EfLandmarkResult Face landmark result structure.
*/
EYEFACE_FUNCTION_PREFIX EfLandmarkResult * efRunFaceLandmark(EfMatrixUc * image, EfDetection * detection, void * module_state);

/*! \fn void efFreeLandmarkResult(EfLandmarkResult * landmark_result) 
  \brief Frees landmark_result structure returned by efRunFaceLandmark().
  \param  landmark_result Pointer to EfLandmarkResult structure to be freed.
*/
EYEFACE_FUNCTION_PREFIX void efFreeLandmarkResult(EfLandmarkResult * landmark_result);

/*! \fn EfGenderAgeResult * efRunFaceGenderAge(EfDetection * detection, void * module_state)
  \brief Runs gender and age recognition.
  \param detection Pointer to face detection structure returned by efRunFaceDetector().
  \param module_state Pointer to EyeFace module state structure received by efInitEyeFace().
  \return EfGenderAgeResult Gender result structure.
*/
EYEFACE_FUNCTION_PREFIX EfGenderAgeResult* efRunFaceGenderAge(EfDetection * detection, void * module_state);

/*! \fn void efFreeGenderAgeResult(EfGenderAgeResult * gender_age_result)
  \brief Frees EfGenderAgeResult.
  \param gender_age_result Pointer to gender and age result structure.
*/
EYEFACE_FUNCTION_PREFIX void efFreeGenderAgeResult(EfGenderAgeResult * gender_age_result);

/*! \fn EfGenderResult * efRunFaceGender(EfDetection * detection, void * module_state)
  \brief Runs gender recognition.
  \param detection Pointer to face detection structure returned by efRunFaceDetector().
  \param module_state Pointer to state structure received by efInitEyeFace(). 
  \return EfGenderResult Gender result structure.
*/
EYEFACE_FUNCTION_PREFIX EfGenderResult * efRunFaceGender(EfDetection * detection, void * module_state);

/*! \fn void efFreeGenderResult(EfGenderResult * gender_result)
  \brief  Frees EfGenderResult.
  \param  gender_result Pointer to gender result structure.
*/
EYEFACE_FUNCTION_PREFIX void efFreeGenderResult(EfGenderResult * gender_result);

/*! \fn EfAgeResult * efRunFaceAge(EfDetection * detection, void * module_state)
  \brief  Runs age recognition.
  \param  detection Pointer to face detection structure returned by efRunFaceDetector().
  \param  module_state Pointer to EyeFace module state structure received by efInitEyeFace(). 
  \return EfAgeResult Age result structure. 
*/
EYEFACE_FUNCTION_PREFIX EfAgeResult * efRunFaceAge(EfDetection * detection, void * module_state);

/*! \fn void efFreeAgeResult(EfAgeResult * age_result)
  \brief  Frees EfAgeResult.
  \param  age_result Pointer to age result structure.
*/
EYEFACE_FUNCTION_PREFIX void efFreeAgeResult(EfAgeResult * age_result);

/*! \fn int efRunFaceImageExtraction(EfMatrixUc * image, EfDetection * detection, const void * module_state, EfLandmarkResult * landmark_result)
    \brief Extracts the face image from the input image, needed for efRunFaceGenderAge() and efRunFaceID().
    \param image Pointer to EfMatrixUc structure containing image data (8-bit unsigned char bitmap).  
    \param detection Pointer to face detection structure returned by efRunFaceDetector().
    \param module_state Pointer to EyeFace module state structure received by efInitEyeFace().
    \param landmark_result Pointer to EfLandmarkResult (returned by efRunFaceLandmark() or NULL to not use landmarks).
    \return -1 on error, otherwise 0.
 */
EYEFACE_FUNCTION_PREFIX int efRunFaceImageExtraction(EfMatrixUc * image, EfDetection * detection, const void * module_state, EfLandmarkResult * landmark_result);

/*! \fn  int efRunFaceIDFeatureExtraction(EfDetection * detection, const void * module_state)
    \brief Computes the ID attributes needed for identity matching
    \param detection Pointer to face detection structure returned by efRunFaceDetector().
    \param module_state Pointer to EyeFace module state structure received by efInitEyeFace().
 */
EYEFACE_FUNCTION_PREFIX int efRunFaceIDFeatureExtraction(EfDetection * detection, const void * module_state);

/*! \fn  int efUpdateTrackState(void * det_result, void * module_state, double time)
    \brief Updates the tracker state after a frame is processed.
    \param det_result Pointer to EfDetResult structure returned by efRunFaceDetector().
    \param module_state Pointer to EyeFace module state structure received by efInitEyeFace().
    \param time Image frame time for tracking purposes (in seconds).
 */
EYEFACE_FUNCTION_PREFIX int efUpdateTrackState(void * det_result, void * module_state, double time);

/*! \fn  int efIDCompareFeatureVectors(const double * sim_a, const double * sim_b, const void * module_state, double * response)
    \brief Compares two identity features sim_a and sim_b.
    \param sim_a EfDetection's (A) feat_id field. 
    \param sim_b EfDetection's (B) feat_id field. 
    \param module_state Pointer to Eyeface SDK module state. 
    \param response Similarity of A to B, higher means more similar.
    \return 0 on success.
 */
EYEFACE_FUNCTION_PREFIX int efIDCompareFeatureVectors(const double * sim_a, const double * sim_b, const void * module_state, double * response);



////////////////////////////
// Public BETA version of color detector - for evaluation purposes
// How to use the color detector:
//      There is a fundamental difference in how to use the beta color detector. If the client does not
//      use the color detector as described, the performance of the detector will be cripled.
//
//      When using grayscale detector, the decision face/non-face is done
//      based on seekout_offset threshold parameter of [SCANNING PARAMETERS] in config.ini.
//      The EfDetResult then contains only those detections, for which confidence exceeds the threshold.
//      By default, the threshold value is 28 and the user typically does not tune the parameter.
//
//      When using color detector, the color is used as a stage of sequential classification,
//      which means the seekout_offset MUST be lowered by the client to zero in config.ini before
//      initializing EyeFace SDK. This way, the weak detections are all boosted by the color classifier.
//      If the seekout_offset is left at 28, there will be no detections to boost by color and the detector will 
//      fail to improve the performance over grayscale. After running efRunFaceDetectorRGB, the client receives 
//      a great number of detections with confidence >= 0. To get the expected results, he must filter out 
//      the EfDetResult by himself using the traditional threshold 28 against confidence member variable of EfDetection, 
//      typically in a for loop.
//      
//      Note: This setup is only for evaluation purposes. The color detector will be fully and seamlessly 
//            integrated in a future release of EyeFace SDK.
//
//////////////////////////////

/*! \fn EfDetResult * efRunFaceDetectorRGB(EfMatrix3Uc * image, void * module_state)

\brief  Runs face detection on a given color image. Requires special setup of EyeFace SDK, read EyeFaceAdvanced.h for details.
\param  image Pointer to EfMatrix3Uc structure containing image data (RGB 8-bit unsigned char bitmap).
\param  module_state Pointer to EyeFace module state structure received by efInitEyeFace().
\return Pointer to EfDetResult structure, NULL on failure.
*/
EYEFACE_FUNCTION_PREFIX EfDetResult * efRunFaceDetectorRGB(EfMatrix3Uc * image, void* module_state);

/*! \fn EfMatrix3Uc * efAllocImage3Uc(int num_rows, int num_cols)
\brief  Allocates RGB 8-bit image bitmap structure (RGBRGBRGB...).
\param  num_rows Number of rows.
\param  num_cols Number of columns.
\return Pointer to EfMatrix3Uc, NULL on failure.
*/
EYEFACE_FUNCTION_PREFIX EfMatrix3Uc * efAllocImage3Uc(int num_rows, int num_cols);

/*! \fn EfMatrixUc * efReadImage3Uc(const char * filename)
\brief  Reads a color image from file (Supports jpegs, tiffs and pngs).
\param  filename Image filename.
\return Pointer to EfMatrixUc, NULL on failure.
*/
EYEFACE_FUNCTION_PREFIX  EfMatrix3Uc * efReadImage3Uc(const char * filename);

/*! \fn void efFreeImage3Uc(EfMatrix3Uc * image)
\brief  Free EfMatrixUc image structure.
\param  image Pointer to EfMatrixUc image structure.
*/
EYEFACE_FUNCTION_PREFIX void efFreeImage3Uc(EfMatrix3Uc * image);
////////////////////////////



/*! @} */



/*! @} */

////////////////////////////////////////////////////////////////////////////////
//
// File I/O functions
//
#if defined(WIN32) || defined(_WIN32) || defined(_DLL) || defined(_WINDOWS_) || defined(_WINDLL)

#define ef_mkdir(A) _mkdir(A)
#define ef_access(A,B) _access(A,B)

#else

#include <unistd.h>
#include <sys/stat.h>
#define ef_mkdir(A) mkdir(A,S_IRWXU)
#define ef_access(A,B) access(A,B)

#endif

#endif


