#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	eyeFace.setup();

	int camWidth 		= 640;	// try to grab at this size. 
	int camHeight 		= 480;
	
    //we can now get back a list of devices. 
	vector<ofVideoDevice> devices = grabber.listDevices();
	
    for(int i = 0; i < devices.size(); i++){
		cout << devices[i].id << ": " << devices[i].deviceName; 
        if( devices[i].bAvailable ){
            cout << endl;
        }else{
            cout << " - unavailable " << endl; 
        }
	}
    
	grabber.setDeviceID(0);
	grabber.setDesiredFrameRate(60);
	grabber.initGrabber(camWidth,camHeight);

	test.loadImage("test_2.jpg");

	font.loadFont("PrivaPro-One.otf", 30, true);

	/*
	ofPixels backtest = eyeFace.backtest();
	test2.setFromPixels(backtest);

	backtest = eyeFace.backtest2();
	test3.setFromPixels(backtest);
	*/

	lastTimeDone = 0;

	gui.setup(); // most of the time you don't need a name
	gui.add(min_stability.setup("min_stability", 0.5, 0.01, 4., 250, 50));
	gui.add(visible_at_least.setup("visible_at_least", 0.5, 0.01, 10., 250, 50));
	gui.add(fps_slider.setup("processing fps", 5, 1, 20, 250, 50));

	

	ui_scale = (float)ofGetWidth() / 640;
}

//--------------------------------------------------------------
void ofApp::update(){
	grabber.update();

	int targetfps = fps_slider;
	float timeframe = 1. / (float)targetfps;

	if ((ofGetElapsedTimef() - lastTimeDone) >= timeframe) {
		//ofLog() << "process...";
		eyeFace.min_stability = min_stability;
		eyeFace.visible_at_least = visible_at_least;
		eyeFace.process(grabber.getTextureReference());
		lastTimeDone = ofGetElapsedTimef();
	}

}

//--------------------------------------------------------------
void ofApp::draw(){
	ofSetColor(255);
	grabber.draw(0,0, 640 * ui_scale, 480 * ui_scale);
	//test.draw(320, 0);

	//test2.draw(0, 200);
	//test3.draw(600, 200);

	ofSetColor(sin(ofGetElapsedTimef()) * 125 + 125, 
		sin(ofGetElapsedTimef() / 2) * 125 + 125,
		sin(ofGetElapsedTimef() * 2) * 125 + 125);
	ofRect(10, 0, 80, 30);

	ofSetColor(255);
	//ofDrawBitmapString(eyeFace.report, 30, 100);
	ofDrawBitmapString(ofToString(ofGetFrameRate()), 20, 20);


	
	for (int i = 0; i < eyeFace.results.size(); i++) {
		ofSetColor(255);
		ofDrawBitmapString("detection_index:" + ofToString(eyeFace.results[i]->detection_index)
			+ " ; " + (eyeFace.results[i]->gender == EF_GENDER_MALE ? " male " : " female ") + 
			" " + ofToString(eyeFace.results[i]->age) + " years (" + 
			ofToString(eyeFace.results[i]->total_time) + ")", 100, 100 + 20 * i);

		ofPushMatrix();
		ofScale(ui_scale, ui_scale);
		ofSetColor(255, 100);
		ofRect(eyeFace.results[i]->current_position.x,
				eyeFace.results[i]->current_position.y,
				eyeFace.results[i]->current_position.width,
				eyeFace.results[i]->current_position.height);
		ofSetColor(sin(ofGetElapsedTimef()) * 125 + 125, 
			sin(ofGetElapsedTimef() / 2) * 125 + 125,
			sin(ofGetElapsedTimef() * 2) * 125 + 125);
		font.drawString(ofToString(eyeFace.results[i]->detection_index),
					eyeFace.results[i]->current_position.x ,
					eyeFace.results[i]->current_position.y + 30);
		ofPopMatrix();

	}

	gui.draw();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
