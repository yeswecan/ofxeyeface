#pragma once

#include "ofMain.h"
#include "ofxEyeFace.h"
#include "ofxGui.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h) {
			ui_scale = (float)w / (float)640;
			ofLog() << "uiscale:" << ui_scale << " (w = " << w << ")";
		};

		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		
		ofxEyeFace eyeFace;

		ofVideoGrabber grabber;

		ofImage test;

		ofImage test2, test3;
		float lastTimeDone;

		ofTrueTypeFont font;

		ofxFloatSlider min_stability, visible_at_least;
		ofxIntSlider fps_slider;

		ofxPanel gui;

		float ui_scale;
};
