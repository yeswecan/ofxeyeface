ofxEyeface
=========

Description
-----------

ofxEyeFace is an openFrameworks wrapper around an EyeFace SDK. 
[http://www.eyedea.cz/eyeface-sdk/](http://www.eyedea.cz/eyeface-sdk/)
